import { Injectable } from '@angular/core';
import {
    HttpInterceptor,
    HttpRequest,
    HttpHandler,
    HttpEvent
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
  constructor() {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const url = 'http://27cce156.ngrok.io/';
    const token: string = sessionStorage.getItem('currentUser');
    if (token) {
			request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + token) });
    }

    request = request.clone({ headers: request.headers.set('Accept', 'application/json') });

    request = request.clone({
      url: url + request.url
    });

    return next.handle(request).pipe(catchError(err => {
      const error = err.error.message || err.statusText;
      return throwError(error);
    }));
  }
}
