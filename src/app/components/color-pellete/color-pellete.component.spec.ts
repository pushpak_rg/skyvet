import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColorPelleteComponent } from './color-pellete.component';

describe('ColorPelleteComponent', () => {
  let component: ColorPelleteComponent;
  let fixture: ComponentFixture<ColorPelleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColorPelleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColorPelleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
