import { Component, OnInit } from '@angular/core';
import { ThemesService } from '../../services/themes/themes.service';

@Component({
  selector: 'app-color-pellete',
  templateUrl: './color-pellete.component.html',
  styleUrls: ['./color-pellete.component.scss']
})
export class ColorPelleteComponent implements OnInit {
  currentTheme: any;
  themesList = ['themeA', 'themeB'];
  constructor(private themes: ThemesService) {
    this.currentTheme = themes.getDefaultTheme();
    this.setTheme(this.currentTheme);
  }

  ngOnInit() {
  }

  setTheme(theme) {
    this.currentTheme = theme;
    this.themes.setTheme(this.currentTheme);
  }
}
