import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from '../../../../services/auth/auth.service';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit {
  protected currentUser: any;
  protected currentUserSubscription: Subscription;
  protected userType = {
    '1001': 'Pet Owner',
    '1002': 'Vet',
    '1003': 'Admin'
  };
  constructor(private authService: AuthService) {
    this.currentUserSubscription = this.authService.currentUser.subscribe(user => {
      this.currentUser = user;
    });
  }

  logout() {
    this.authService.logout();
  }

  ngOnInit() {
  }

}
