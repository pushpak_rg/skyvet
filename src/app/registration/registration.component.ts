import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService } from '../services/alert/alert.service';
import { RegisterService } from '../services/register/register.service';
import { PasswordValidationService } from '../services/PasswordValidation/password-validation.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  private registerForm: FormGroup;
  protected loading = false;
  protected submitted = false;
  private returnUrl: string;
  private emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
  private selectedTab: Number = 1;
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private alertService: AlertService,
    private registerService: RegisterService,
    private passwordValidationService: PasswordValidationService) { }

  ngOnInit() {
    this.createForm();
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

    if (sessionStorage.getItem('authToken')) {
      this.router.navigateByUrl(this.returnUrl);
    }
  }

  onTabChange(tab: Number) {
    this.selectedTab = tab;
    this.createForm();
  }

  createForm() {
    if (this.selectedTab === 2) {
      this.registerForm = this.formBuilder.group({
        email: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
        fullName: ['', [ Validators.required, Validators.minLength(5)]],
        contactNo: ['', Validators.required],
        password: ['', [Validators.required, Validators.minLength(6)]],
        confirmPassword: ['', Validators.required],
        licenseNo: ['', Validators.required],
        recepientFax: ['', Validators.required]
      });
    } else {
      this.registerForm = this.formBuilder.group({
        email: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
        fullName: ['', [ Validators.required, Validators.minLength(5)]],
        contactNo: ['', Validators.required],
        password: ['', [Validators.required, Validators.minLength(6)]],
        confirmPassword: ['', Validators.required]
      });
    }
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  checkPasswords(group: FormGroup) {
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirmPassword.value;

    return pass === confirmPass ? null : { notSame: true }     
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    const userDetails = {
      'login': this.f.email.value,
      'password': this.f.password.value,
      'firstName': this.f.fullName.value,
      'lastName': this.f.fullName.value,
      'contactNo': this.f.contactNo.value,
      'email': this.f.email.value,
      'userType': '1001',
      'zoneId': Intl.DateTimeFormat().resolvedOptions().timeZone
    };
    if (this.selectedTab === 2) {
      userDetails['licenseNo'] = this.f.licenseNo.value;
      userDetails['recepientFax'] = this.f.recepientFax.value;
      userDetails['userType'] = '1002';
    } else if (this.selectedTab === 3) {
      userDetails['userType'] = '1003';
    }
    this.loading = true;
    this.registerService.register(userDetails)
    .subscribe((user) => {
      this.router.navigateByUrl('/login');
      this.alertService.success('User account created successfully! Please Login');
    },
    error => {
      this.alertService.error(error);
      this.loading = false;
    });
  }

}
