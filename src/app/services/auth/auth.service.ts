import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AuthService {
  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;
  constructor(private http: HttpClient, private routes: Router) {
    this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(sessionStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue() {
    return this.currentUserSubject.value;
  }

  login(username: string, password: string) {
    return this.http.post<any>('das/api/authenticate', { username, password })
      .pipe(map(user => {
        this.currentUserSubject.next(user);
        return user;
      }));
  }

  logout() {
    sessionStorage.removeItem('currentUser');
    sessionStorage.removeItem('authToken');
    this.currentUserSubject.next(null);
    this.routes.navigate(['login']);
  }
}
