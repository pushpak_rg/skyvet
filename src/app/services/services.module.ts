import { NgModule } from '@angular/core';
import { AppComponent } from '../app.component';

import { AlertService } from './alert/alert.service';
import { AuthService } from './auth/auth.service';
import { RegisterService } from './register/register.service';
import { HttpClientModule } from '@angular/common/http';
import { ThemesService } from './themes/themes.service';

@NgModule({
	imports: [
		HttpClientModule
	],
	providers: [
		AlertService,
		AuthService,
		RegisterService,
		ThemesService
	],
	bootstrap: [AppComponent]
})

export class ServicesModule { }
