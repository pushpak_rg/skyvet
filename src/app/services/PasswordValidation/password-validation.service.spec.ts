import { TestBed } from '@angular/core/testing';

import { PasswordValidationService } from './password-validation.service';

describe('PasswordValidationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PasswordValidationService = TestBed.get(PasswordValidationService);
    expect(service).toBeTruthy();
  });
});
