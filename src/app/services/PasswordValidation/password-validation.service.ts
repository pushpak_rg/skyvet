import { Injectable } from '@angular/core';
import {AbstractControl} from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class PasswordValidationService {

  constructor() { }

  MatchPassword(AC: AbstractControl) {
    let password = AC.get('password').value;
    let confirmPassword = AC.get('confirmPassword').value;
    if (password) {
        if(password != confirmPassword) {
            AC.get('confirmPassword').setErrors( {matchPassword: true} )
        } else {
            AC.get('confirmPassword').setErrors( {matchPassword: false} )
            return null
        }
    } else {
        AC.get('confirmPassword').setErrors( {required: true, matchPassword: false} )
        return null
    }
 }
}
