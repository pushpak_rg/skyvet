import { Injectable } from '@angular/core';

const themeA = require('../../shared/styles/themes/theme-a.scss');
const themeB = require('../../shared/styles/themes/theme-b.scss');

@Injectable()
export class ThemesService {
    private styleTag: any;
    private defaultTheme: string;

    constructor() {
        this.defaultTheme = this.getTheme();
        this.createStyle();
        this.setTheme(this.defaultTheme);
    }

    private getTheme() {
        if (localStorage.getItem('userTheme')) {
            return localStorage.getItem('userTheme');
        } else {
            localStorage.setItem('userTheme', 'themeA');
            return localStorage.getItem('userTheme');
        }
    }

    private createStyle() {
        const head = document.head || document.getElementsByTagName('head')[0];
        this.styleTag = document.createElement('style');
        this.styleTag.type = 'text/css';
        this.styleTag.id = 'appthemes';
        head.appendChild(this.styleTag);
    }

    setTheme(name: string) {
        switch (name) {
            case 'themeA':
                this.injectStylesheet(themeA);
                localStorage.setItem('userTheme', 'themeA');
                break;
            case 'themeB':
                this.injectStylesheet(themeB);
                localStorage.setItem('userTheme', 'themeB');
                break;
        }
    }

    private injectStylesheet(css) {
        this.styleTag.innerHTML = css;
    }

    getDefaultTheme() {
        return this.defaultTheme;
    }
}
