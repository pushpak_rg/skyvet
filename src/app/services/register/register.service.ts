import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  constructor(
    private http: HttpClient) { }

  register(user: any) {
    return this.http.post<any>('das/api/register', user)
      .pipe(map((user) => user));
  }
}
