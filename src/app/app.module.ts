import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopbarComponent } from './shared/layout/header/topbar/topbar.component';
import { MainbarComponent } from './shared/layout/header/mainbar/mainbar.component';
import { HeaderComponent } from './shared/layout/header/header.component';
import { LayoutComponent } from './shared/layout/layout.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AlertComponent } from './components/alert/alert/alert.component';
import { ServicesModule } from './services/services.module';
import { AuthGuard } from './guards/guards.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpConfigInterceptor} from './interceptor/httpconfig/httpconfig.interceptor';
import { RegistrationComponent } from './registration/registration.component';
import { ColorPelleteComponent } from './components/color-pellete/color-pellete.component';

@NgModule({
  declarations: [
    AppComponent,
    TopbarComponent,
    MainbarComponent,
    HeaderComponent,
    LayoutComponent,
    LoginComponent,
    HomeComponent,
    AlertComponent,
    RegistrationComponent,
    ColorPelleteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ServicesModule
  ],
  providers: [
    AuthGuard,
    { provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
